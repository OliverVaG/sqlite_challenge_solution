import csv
import json
import sqlite3


with open('Inventories.csv') as data_file:
    reader = csv.reader(data_file)
    next(reader)
    row_n = 1
    for row in reader:
        print(row)
        this_id = row[0]
        organization_id = row[1]
        created_by = row[2]
        timestamp = row[3]
        name = row[4]
        this_type = row[5]
        variety = row[6]

        index = 7
        entire_row = row[index]
        while index < len(row)-1:
            entire_row = entire_row + ", " + row[index + 1]
            index += 1

        clean = ((entire_row.replace("\"", "")).replace(" ", "")).replace(",,", "")
        if clean[-1:] == ",":
            clean = clean[:-1]

        my_list = "[" + clean + "]"
        list_form = json.loads(("[" + clean + "]").replace("\'", "\""))
        row_n += 1

        data = json.dumps(list_form[0])
        stats = json.dumps(list_form[1])
        attributes = json.dumps(list_form[2])

        connection = sqlite3.connect('DatabaseChallenge.db')
        cursor = connection.cursor()
        insert = "INSERT OR IGNORE INTO inventories(id, organization_id, created_by, timestamp, name, type, variety, data, stats, attributes) VALUES(?,?,?,?,?,?,?,?,?,?);"
        values = [this_id, organization_id, created_by, timestamp, name, this_type, variety, str(data), str(stats), str(attributes)]
        cursor.execute(insert, values)
        connection.commit()


with open('Activities.csv') as data_file:
    reader = csv.reader(data_file)
    next(reader)

    row_iter = 1
    for row in reader:
        # print(row)
        activity_id = row[0]
        organization_id = row[1]
        created_by = row[2]
        timestamp = row[3]
        name = row[4]

        index = 5
        entire_row = row[index]
        while index < len(row)-1:
            entire_row = entire_row + ", " + row[index + 1]
            index += 1

        clean = ((entire_row.replace("\"", "")).replace(" ", "")).replace(",,", "")
        if clean[-1:] == ",":
            clean = clean[:-1]
        my_list = "[" + clean + "]"

        try:
            print(clean)
            list_form = json.loads((("[" + clean + "]").replace("\'", "\"")))
            data = json.dumps(list_form[0])
        except json.decoder.JSONDecodeError as error:
            data = json.dumps(clean)
        row_iter += 1

        connection = sqlite3.connect('DatabaseChallenge.db')
        cursor = connection.cursor()
        insert = "INSERT OR IGNORE INTO activities(id, organization_id, created_by, timestamp, name, data) VALUES(?,?,?,?,?,?);"
        values = [activity_id, organization_id, created_by, timestamp, name, data]
        cursor.execute(insert, values)
        connection.commit()


