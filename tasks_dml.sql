CREATE TABLE IF NOT EXISTS activities(
    id NUMERIC PRIMARY KEY,
    organization_id NUMERIC,
    created_by NUMERIC,
    timestamp TIMESTAMP,
    name TEXT,
    data TEXT
);

CREATE TABLE IF NOT EXISTS inventories(
    id NUMERIC PRIMARY KEY,
    organization_id NUMERIC,
    created_by NUMERIC,
    timestamp TIMESTAMP,
    name TEXT,
    type TEXT,
    variety TEXT,
    data TEXT,
    stats TEXT,
    attributes TEXT
);

-- TASK 1
-- Find all the activities pertaining to inventory ID 85691. Provide the query to achieve this.
SELECT * FROM activities WHERE data LIKE '%85691%' ORDER BY timestamp; -- query to achieve this.


-- TASK 2
--For inventory ID 87516, the client made a mistake, instead of 98 plants, it should have been 85 plants.
-- Identify the chain of activities that need to be changed – Provide an SQL query.
-- Provide a SQL query to affect the required changes.
SELECT * FROM activities WHERE data LIKE '%87516%'; -- chain of activities that need to be changed.
UPDATE activities SET data = REPLACE(data, '98', '85') WHERE data LIKE '%87516%'; -- query to affect the required changes.


-- TASK 3
-- For inventory ID 83209 that were completely destroyed, the client would like to backdate to September 28th, instead of October 1st.
-- Each inventory movement, whether receiving or destruction, generates an inventory adjustment activity
-- Identify the chain of activities that need to be changed and provide a SQL query to affect the required changes.
SELECT * FROM activities WHERE data LIKE '%83209%'; -- chain of activities that need to be changed.
UPDATE activities SET timestamp = REPLACE(timestamp, '10-01', '09-28') WHERE data LIKE '%83209%'; -- query to affect the required changes.
