### Steps followed:
1. Export XLSX file into CSV.
2. Write Python script to read CSV file and insert the data into an SQLite database.
3. Create the tables to insert the data.
3. Analyze data and write the queries for the tasks.

Step 2 was actually the complicated part (90% of the time) since the rows exported from the XLS into CSV were full of quotes, double quotes and commas that made it tricky to bring back into a valid JSON, and also the activities table was quite slow to insert.